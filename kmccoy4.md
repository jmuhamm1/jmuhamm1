Hi, I'm Kelley Deuso! I'm a Masters student in Computer Science, and
I will be graduating this December. My focus for my Masters has been
supercomputing and data centers, and I was part of a class this summer
that built a 68-node RaspberryPi3 pseudo-supercomputer. Some fun facts
about me, I love cats, cars, comedy, and computers!